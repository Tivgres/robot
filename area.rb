# frozen_string_literal: true

# Here we must setup area for playing.
class Area
  attr_accessor :area_size_x, :area_size_y

  def initialize
    puts 'Are u wanna use default size 5x6? (Y/n)'
    if gets.chomp.to_s == 'n'
      show_delimiter
      puts "#{'#' * 3} Setting size manually #{'#' * 3}"
      show_delimiter
      @area_size_x = coordinates_manager 'X'
      @area_size_y = coordinates_manager 'Y'
    else
      @area_size_x = 5
      @area_size_y = 6
    end
  end

  private

  def show_delimiter
    puts '#' * 30
  end

  def coordinates_manager(coordinate_type)
    puts "Enter bellow size of #{coordinate_type} coordinates:"
    area_size = gets.chomp.to_i
    while area_size < 1
      wrong_input
      area_size = ask_size coordinate_type
    end
    area_size
  end

  def wrong_input
    puts 'Wrong input, try again'
  end

  def ask_size(coordinates)
    puts "Enter bellow #{coordinates} size:"
    gets.chomp.to_i
  end
end
