# frozen_string_literal: true

# First time we setup robot position
# After this we can play, and here all robot logic about game
class Robot
  attr_accessor :max_area_x, :max_area_y, :position_x, :position_y, :side, :available_sides, :area

  def initialize(area)
    @available_sides = %w[SOUTH EAST NORTH WEST]
    @area = area
    @max_area_x = @area.area_size_x
    @max_area_y = @area.area_size_y
    show_delimiter
    puts "Now u must set robot position:
           (PLACE X,Y,F)
           (PLACE 0,0,NORTH)"
    show_delimiter
    setup_start_position
  end

  def move
    case @side
    when @available_sides[0]
      @position_y -= 1 if @position_y != 0
    when @available_sides[1]
      @position_x += 1 if @position_x != (@max_area_x - 1)
    when @available_sides[2]
      @position_y += 1 if @position_y != (@max_area_y - 1)
    when @available_sides[3]
      @position_x -= 1 if @position_x != 0
    end
  end

  def left
    change_position false
  end

  def right
    change_position true
  end

  def report
    puts "Now I'm here"
    puts "#{@position_x} #{@position_y} #{@side}"
  end

  private

  def setup_start_position
    user_x = -1
    user_y = -1
    user_side = ''
    until valid_position? user_x, user_y, user_side
      user_input = gets.chomp.to_s.split(',')
      next unless user_input.size == 3
      user_x = user_input[0].to_i
      user_y = user_input[1].to_i
      user_side = user_input[2].to_s.upcase
    end
    set_robot_position user_x, user_y, user_side
  end

  def valid_position?(user_x, user_y, user_side)
    return false if user_x.negative? && user_x > @max_area_x
    return false if user_y.negative? && user_y > @max_area_y
    return false unless @available_sides.include? user_side
    true
  end

  def set_robot_position(position_x, position_y, side)
    @position_x = position_x
    @position_y = position_y
    @side = side
  end

  def show_delimiter
    puts '#' * 30
  end

  def change_position(to_right)
    current_pos = @available_sides.index(side)
    @side = if to_right
              current_pos != 0 ? @available_sides[(current_pos -= 1)] : @available_sides[3]
            else
              current_pos != 3 ? @available_sides[(current_pos += 1)] : @available_sides[0]
                end
  end
end
