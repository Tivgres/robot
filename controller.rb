# frozen_string_literal: true

require_relative 'area'
require_relative 'robot'

# Inside this class works all magic for manage  area and one robot
# After setup all settings we will play
class Controller
  attr_accessor :last_command
  def initialize
    area = Area.new
    robot = Robot.new area
    show_help
    until @last_command == 'EXIT'
      puts 'PUT YOUR NEXT COMMAND'
      @last_command = gets.chomp.to_s.upcase
      case @last_command
      when 'MOVE'
        robot.move
      when 'LEFT'
        robot.left
      when 'RIGHT'
        robot.right
      when 'REPORT'
        robot.report
      else
        puts @last_command != 'EXIT' ? 'Wrong input, try again!' : 'Bye-bye'
      end
    end
  end

  private

  def show_help
    puts "AVAILABLE COMMANDS:
    MOVE
    LEFT
    RIGHT
    REPORT
    EXIT"
  end
end

Controller.new
